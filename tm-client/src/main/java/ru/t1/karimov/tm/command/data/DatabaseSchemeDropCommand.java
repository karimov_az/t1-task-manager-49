package ru.t1.karimov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.domain.DatabaseSchemeDropRequest;
import ru.t1.karimov.tm.enumerated.Role;

public final class DatabaseSchemeDropCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Drop database scheme.";

    @NotNull
    public static final String NAME = "database-drop-scheme";

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().dropDatabaseScheme(new DatabaseSchemeDropRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
