package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.project.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    public static final String NAME = "project-clear";

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CLEAR]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        getProjectEndpoint().clearProjects(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
