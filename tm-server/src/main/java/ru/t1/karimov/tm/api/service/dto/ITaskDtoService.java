package ru.t1.karimov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.enumerated.Status;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDto> {

    @NotNull
    TaskDto changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    TaskDto changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    TaskDto create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    TaskDto create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    List<TaskDto> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws Exception;

    @NotNull
    TaskDto updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    TaskDto updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
