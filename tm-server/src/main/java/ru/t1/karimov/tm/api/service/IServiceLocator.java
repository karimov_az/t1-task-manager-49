package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.service.dto.IProjectDtoService;
import ru.t1.karimov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.karimov.tm.api.service.dto.ITaskDtoService;
import ru.t1.karimov.tm.api.service.dto.IUserDtoService;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    IProjectTaskDtoService getProjectTaskService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IUserDtoService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}
