package ru.t1.karimov.tm.api.service.dto;

import ru.t1.karimov.tm.dto.model.SessionDto;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDto> {
}
