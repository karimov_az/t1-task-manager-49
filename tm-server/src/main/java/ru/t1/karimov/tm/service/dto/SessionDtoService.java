package ru.t1.karimov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.dto.model.SessionDto;
import ru.t1.karimov.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

public class SessionDtoService extends AbstractUserOwnedDtoService<SessionDto> implements ru.t1.karimov.tm.api.service.dto.ISessionDtoService {

    public SessionDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

}
