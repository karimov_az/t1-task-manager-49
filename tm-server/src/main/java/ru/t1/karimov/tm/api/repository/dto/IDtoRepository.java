package ru.t1.karimov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.AbstractDtoModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IDtoRepository<M extends AbstractDtoModel> {

    @NotNull
    M add(@NotNull M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    Long getSize() throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull Integer index) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    void removeAll() throws Exception;

    void removeOne(@NotNull M model) throws Exception;

    void removeOneById(@NotNull String id) throws Exception;

    void removeOneByIndex(@NotNull Integer index) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    void update(@NotNull M model) throws Exception;

}

