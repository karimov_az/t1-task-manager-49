package ru.t1.karimov.tm.migration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public abstract class AbstractSchemeTest {

    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    private static Database DATABASE;

    @BeforeClass
    public static void before() throws IOException, DatabaseException, SQLException {
        @NotNull final Properties properties = new Properties();
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        @NotNull final Connection connection = getConnection(properties);
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    protected static Liquibase liquibase(@NotNull final String fileName) {
        return new Liquibase(fileName, ACCESSOR, DATABASE);
    }

    private static Connection getConnection(@NotNull final Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

    @AfterClass
    public static void after() throws DatabaseException {
        DATABASE.close();
    }

}
