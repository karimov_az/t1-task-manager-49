package ru.t1.karimov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {
    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(@NotNull final String value) {
        if (value.isEmpty()) return null;
        for (@NotNull final Status status: values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

    @Getter
    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
