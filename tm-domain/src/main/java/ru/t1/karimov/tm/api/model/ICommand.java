package ru.t1.karimov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ICommand {

    void execute() throws Exception;

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

}
